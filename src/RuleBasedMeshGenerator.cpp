/*
 * RuleBasedMeshGenerator.cpp
 *
 *  Created on: 29 cze 2013
 *      Author: Revers
 */

#include <iostream>
#include <GL/glew.h>

#include <QDesktopWidget>
#include <QApplication>
#include <QGLFormat>

#include <glm/glm.hpp>

#include <rev/common/RevConfigReader.h>
#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>

#include "rev/qt/MainWindow.h"

using namespace std;
using namespace rev;

void center(QWidget &widget) {

	int width = widget.width();
	int height = widget.height();

	QDesktopWidget* desktop = QApplication::desktop();

	int screenWidth = desktop->width();
	int screenHeight = desktop->height();

	int x = (screenWidth - width) / 2;
	int y = (screenHeight - height) / 2;

	widget.setGeometry(x, y, width, height);
}

#define REV_PROPERTIES "QtRuleBasedMeshGenerator.properties"

int main(int argc, char* argv[]) {
	if (!rev::ConfigReader::load(REV_PROPERTIES)) {
		REV_ERROR_MSG("Failed loading '" REV_PROPERTIES "' file!");
	}
	rev::ResourcePath::init(argc, argv);

	QApplication app(argc, argv);
	app.setApplicationName("RuleBasedMeshGenerator");
	app.setOrganizationName("revers");

	// The GL view
	QGLFormat format;
	format.setVersion(3, 2);
	format.setProfile(QGLFormat::CoreProfile);
	format.setSampleBuffers(true);
	format.setSamples(16);

	MainWindow window(format);
	window.show();
	center(window);

	window.init();

	int qtStatus = app.exec();
	return qtStatus;
}

