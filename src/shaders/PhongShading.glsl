/**
 * Phong Shading Shader
 */
#include <FAKE_INCLUDE/glsl_fake_new.hxx>
//$END_FAKE$
//---------------------------------------------------------------------
//-- Vertex
#version 150
in vec3 VertexPosition;
in vec3 VertexNormal;

out vec3 Position;
out vec3 Normal;

uniform mat4 ModelViewMatrix;
uniform mat3 NormalMatrix;
uniform mat4 MVP;

void main() {
	Normal = normalize(NormalMatrix * VertexNormal);
	Position = vec3(ModelViewMatrix * vec4(VertexPosition, 1.0));

	gl_Position = MVP * vec4(VertexPosition, 1.0);
}
//---------------------------------------------------------------------
//-- Fragment
#version 150

in vec3 Position;
in vec3 Normal;

uniform vec4 LightPosition;
uniform vec3 LightColor;
uniform vec3 Kd; // Diffuse reflectivity
uniform vec3 Ka; // Ambient reflectivity
uniform vec3 Ks; // Specular reflectivity

uniform float Shininess; // Specular shininess factor

out vec4 FragColor;

vec3 ads() {
	vec3 s = normalize(vec3(LightPosition) - Position);
	vec3 v = normalize(vec3(-Position));
	vec3 r = reflect(-s, Normal);

	return LightColor * (Ka +
			Kd * max(dot(s, Normal), 0.0) +
			Ks * pow(max(dot(r, v), 0.0), Shininess));
}

void main() {
	FragColor = vec4(ads(), 1.0);
}
//---------------------------------------------------------------------
