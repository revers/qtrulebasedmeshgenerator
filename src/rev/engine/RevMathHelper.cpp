/*
 * RevMathHelper.cpp
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#include "RevMathHelper.h"

using namespace rev;

const glm::vec3 MathHelper::FORWARD(0.0, 0.0, 1.0);
const glm::vec3 MathHelper::UP(0.0, 1.0, 0.0);
const glm::vec3 MathHelper::RIGHT(1.0, 0.0, 0.0);
const glm::vec3 MathHelper::BACK(0.0, 0.0, -1.0);
const glm::vec3 MathHelper::DOWN(0.0, -1.0, 0.0);
const glm::vec3 MathHelper::LEFT(-1.0, 0.0, 0.0);

const glm::vec3 MathHelper::ZERO(0.0, 0.0, 0.0);
const glm::vec3 MathHelper::ONE(1.0, 1.0, 1.0);

/**
 * Decomposes matrix M such that T * R * S = M, where T is translation matrix,
 * R is rotation matrix and S is scaling matrix.
 * http://code.google.com/p/assimp-net/source/browse/trunk/AssimpNet/Matrix4x4.cs
 * (this method is exact to at least 0.0001f)
 *
 * | 1  0  0  T1 | | R11 R12 R13 0 | | a 0 0 0 |   | aR11 bR12 cR13 T1 |
 * | 0  1  0  T2 |.| R21 R22 R23 0 |.| 0 b 0 0 | = | aR21 bR22 cR23 T2 |
 * | 0  0  0  T3 | | R31 R32 R33 0 | | 0 0 c 0 |   | aR31 bR32 cR33 T3 |
 * | 0  0  0   1 | |  0   0   0  1 | | 0 0 0 1 |   |  0    0    0    1 |
 *
 * @param m (in) matrix to decompose
 * @param scaling (out) scaling vector
 * @param rotation (out) rotation matrix
 * @param translation (out) translation vector
 */
void rev::MathHelper::decomposeTRS(const glm::mat4& m, glm::vec3& scaling,
        glm::mat4& rotation, glm::vec3& translation) {
    // Extract the translation
    translation.x = m[3][0];
    translation.y = m[3][1];
    translation.z = m[3][2];

    // Extract col vectors of the matrix
    glm::vec3 col1(m[0][0], m[0][1], m[0][2]);
    glm::vec3 col2(m[1][0], m[1][1], m[1][2]);
    glm::vec3 col3(m[2][0], m[2][1], m[2][2]);

    //Extract the scaling factors
    scaling.x = glm::length(col1);
    scaling.y = glm::length(col2);
    scaling.z = glm::length(col3);

    // Handle negative scaling
    if (glm::determinant(m) < 0) {
        scaling.x = -scaling.x;
        scaling.y = -scaling.y;
        scaling.z = -scaling.z;
    }

    // Remove scaling from the matrix
    if (scaling.x != 0) {
        col1 /= scaling.x;
    }

    if (scaling.y != 0) {
        col2 /= scaling.y;
    }

    if (scaling.z != 0) {
        col3 /= scaling.z;
    }

    rotation[0][0] = col1.x;
    rotation[0][1] = col1.y;
    rotation[0][2] = col1.z;
    rotation[0][3] = 0.0;

    rotation[1][0] = col2.x;
    rotation[1][1] = col2.y;
    rotation[1][2] = col2.z;
    rotation[1][3] = 0.0;

    rotation[2][0] = col3.x;
    rotation[2][1] = col3.y;
    rotation[2][2] = col3.z;
    rotation[2][3] = 0.0;

    rotation[3][0] = 0.0;
    rotation[3][1] = 0.0;
    rotation[3][2] = 0.0;
    rotation[3][3] = 1.0;
}

/**
 * Decompose matrix M, such that M = QR, where Q is an orthogonal matrix, and R is some matrix.
 * http://www.keithlantz.net/2012/05/qr-decomposition-using-householder-transformations/
 *
 * @param M matrix to decompose.
 * @param Q out parameter
 * @param R out parameter
 */
void rev::MathHelper::decomposeQR(const glm::mat4& M, glm::mat4& Q,
        glm::mat4& R) {
    double mag, alpha;
    glm::vec4 u, v;
    glm::mat4 P(1.0);
    glm::mat4 I(1.0);

    Q = glm::mat4(1.0);
    R = M;

    for (int i = 0; i < 4; i++) {
        u = glm::vec4(0.0);
        v = glm::vec4(0.0);

        mag = 0.0;
        for (int j = i; j < 4; j++) {
            u[j] = R[j][i];
            mag += u[j] * u[j];
        }
        mag = sqrt(mag);

        alpha = u[i] < 0 ? mag : -mag;

        mag = 0.0;
        for (int j = i; j < 4; j++) {
            v[j] = j == i ? u[j] + alpha : u[j];
            mag += v[j] * v[j];
        }
        mag = sqrt(mag);

        if (mag < EPSILON_D)
            continue;

        for (int j = i; j < 4; j++)
            v[j] /= mag;

        P = I - (multTranspose(v, v)) * 2.0;

        R = P * R;
        Q = Q * P;
    }
}

/**
 * Decompose matrix M, such that M = RQ, where Q is an orthogonal matrix, and R is some matrix.
 * http://www.keithlantz.net/2012/05/qr-decomposition-using-householder-transformations/
 *
 * @param M matrix to decompose.
 * @param Q out parameter
 * @param R out parameter
 */
void rev::MathHelper::decomposeRQ(const glm::mat4& M, glm::mat4& Q,
        glm::mat4& R) {
    double mag, alpha;

    glm::vec4 u, v;
    glm::mat4 P(1.0), I(1.0);

    Q = glm::mat4(1.0);
    R = M;

    for (int i = 0; i < 4; i++) {
        u = glm::vec4(0.0);
        v = glm::vec4(0.0);

        mag = 0.0;
        for (int j = i; j < 4; j++) {
            u[j] = R[j][i];
            mag += u[j] * u[j];
        }
        mag = sqrt(mag);

        alpha = u[i] < 0 ? mag : -mag;

        mag = 0.0;
        for (int j = i; j < 4; j++) {
            v[j] = j == i ? u[j] + alpha : u[j];
            mag += v[j] * v[j];
        }
        mag = sqrt(mag);

        if (mag < EPSILON_D)
            continue;

        for (int j = i; j < 4; j++)
            v[j] /= mag;

        P = I - (multTranspose(v, v)) * 2.0;

        R = R * P;
        Q = P * Q;

    }
}

glm::mat4 rev::MathHelper::multTranspose(const glm::vec4& A,
        const glm::vec4& B) {
    glm::mat4 R;
    for (int j = 0; j < 4; j++) {
        for (int i = 0; i < 4; i++) {
            R[j][i] = A[j] * B[i];
        }
    }
    return R;
}

/**
 * Decomposes glm::perspective() matrix.
 *
 * @param p      [in]  a glm::perspective() matrix.
 * @param fovy   [out] angle
 * @param aspect [out] aspect ratio (width / height)
 * @param near_  [out] near plane
 * @param far_   [out] far plane
 */
void rev::MathHelper::decomposePerspective(const glm::mat4& p,
        float& fovy, float& aspect, float& near_, float& far_) {
    double nearD = (double) p[3][2] / ((double) p[2][2] - 1.0);
    double farD = (double) p[3][2] / ((double) p[2][2] + 1.0);

    double rightD = nearD / (double) p[0][0];
    double arD = rightD * ((double) p[1][1] / nearD);
    double fovyD = glm::degrees(atan(1.0 / (double) p[1][1]) * 2.0);

    fovy = (float) fovyD;
    aspect = (float) arD;
    near_ = (float) nearD;
    far_ = (float) farD;
}
