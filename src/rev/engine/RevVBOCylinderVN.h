/*
 * RevVBOCylinderVN.h
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 */

#ifndef REVVBOCYLINDERVN_H_
#define REVVBOCYLINDERVN_H_

#include <glm/glm.hpp>
#include "RevIDrawable.h"

namespace rev {

    class VBOCylinderVN: public IDrawable {
        static const int HANDLE_SIZE = 3;
        static const int HANDLE_VERTEX = 0;
        static const int HANDLE_NORMAL = 1;
        static const int HANDLE_ELEMENT = 2;
        unsigned int vaoHandle;
        unsigned int handle[HANDLE_SIZE];
        int indices = 0;

        float radius;
        float height;
        int tessellation;

        int infoVertices = 0;
        int infoTriangles = 0;

        VBOCylinderVN();

    public:

        VBOCylinderVN(float height, float radius, int tessellation);
        virtual ~VBOCylinderVN();

        void render();

        int getPolygonCount() {
            return infoTriangles;
        }

    private:
        bool generate();

        void createCap(int tessellation,
                float height,
                float radius,
                const glm::vec3& normal,
                glm::vec3* v,
                glm::vec3* n,
                unsigned int* el,
                int& vIndex,
                int& nIndex,
                int& elIndex);

        glm::vec3 getCircleVector(int i, int tessellation);
    };

} /* namespace rev */
#endif /* REVVBOCYLINDERVN_H_ */
