/*
 * RevICamera.h
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#ifndef REVICAMERA_H_
#define REVICAMERA_H_

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>

namespace rev {

    class ICamera {

    public:
        virtual ~ICamera() {
        }

        virtual const glm::vec3& getPosition() const = 0;
        virtual const glm::vec3& getDirection() const = 0;
        virtual const glm::mat4& getViewMatrix() const = 0;
        virtual const glm::mat4& getProjectionMatrix() const = 0;
        virtual void setScreenSize(float width, float height) = 0;

        static glm::mat4 createDefalutProjectionMatrix(float width, float height) {
            return glm::perspective(60.0f, width / height,
                    0.3f, 10000.0f);
        }
    };

}

#endif /* REVICAMERA_H_ */
