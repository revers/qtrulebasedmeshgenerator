#include <GL/glew.h>
#include <GL/gl.h>

#include "RevVBOCubeVNT.h"
#include <rev/gl/RevGLDefines.h>
#include <rev/gl/RevGLUtil.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevAssert.h>

using namespace rev;

VBOCubeVNT::VBOCubeVNT(float side) :
		side(side) {
	glGenBuffers(4, handle);
	glAssert;

	clearVAOHandles(REV_ENGINE_MAX_CONTEXTS);

	bool succ = generate();
	revAssert(succ);
}

void VBOCubeVNT::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
	int index = GLContextManager::ref().getCurrentContextIndex();
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif
	glDrawElements(GL_TRIANGLES, 36, GL_UNSIGNED_INT, ((GLubyte *) NULL + (0)));
	glBindVertexArray(0);
}

VBOCubeVNT::~VBOCubeVNT() {
	glDeleteBuffers(4, handle);
}

void VBOCubeVNT::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}

	glBindVertexArray(vaoHandle[contextIndex]);

	//-------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
	glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(0); // Vertex position
	//-------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
	glVertexAttribPointer((GLuint) 1, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(1); // Vertex normal
	//-------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
	glVertexAttribPointer((GLuint) 2, 2, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(2); // texture coords
	//-------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
	//-------------------------------------------------------------
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

bool VBOCubeVNT::generate() {
	float side2 = side / 2.0f;

//	float v[24 * 3] = {
//				// Front
//				0.0f, 0.0f, side,
//				side, 0.0f, side,
//				side, side, side,
//				0.0f, side, side,
//				// Right
//				side, 0.0f, side,
//				side, 0.0f, 0.0f,
//				side, side, 0.0f,
//				side, side, side,
//				// Back
//				0.0f, 0.0f, 0.0f,
//				0.0f, side, 0.0f,
//				side, side, 0.0f,
//				side, 0.0f, 0.0f,
//				// Left
//				0.0f, 0.0f, side,
//				0.0f, side, side,
//				0.0f, side, 0.0f,
//				0.0f, 0.0f, 0.0f,
//				// Bottom
//				0.0f, 0.0f, side,
//				0.0f, 0.0f, 0.0f,
//				side, 0.0f, 0.0f,
//				side, 0.0f, side,
//				// Top
//				0.0f, side, side,
//				side, side, side,
//				side, side, 0.0f,
//				0.0f, side, 0.0f
//		};

	float v[24 * 3] = {
			// Front
			-side2, -side2, side2,
			side2, -side2, side2,
			side2, side2, side2,
			-side2, side2, side2,
			// Right
			side2, -side2, side2,
			side2, -side2, -side2,
			side2, side2, -side2,
			side2, side2, side2,
			// Back
			-side2, -side2, -side2,
			-side2, side2, -side2,
			side2, side2, -side2,
			side2, -side2, -side2,
			// Left
			-side2, -side2, side2,
			-side2, side2, side2,
			-side2, side2, -side2,
			-side2, -side2, -side2,
			// Bottom
			-side2, -side2, side2,
			-side2, -side2, -side2,
			side2, -side2, -side2,
			side2, -side2, side2,
			// Top
			-side2, side2, side2,
			side2, side2, side2,
			side2, side2, -side2,
			-side2, side2, -side2
	};

	float n[24 * 3] = {
			// Front
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			0.0f, 0.0f, 1.0f,
			// Right
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			1.0f, 0.0f, 0.0f,
			// Back
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			0.0f, 0.0f, -1.0f,
			// Left
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			-1.0f, 0.0f, 0.0f,
			// Bottom
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			0.0f, -1.0f, 0.0f,
			// Top
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f,
			0.0f, 1.0f, 0.0f
	};

	float tex[24 * 2] = {
			// Front
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			// Right
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			// Back
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			// Left
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			// Bottom
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f,
			// Top
			0.0f, 0.0f,
			1.0f, 0.0f,
			1.0f, 1.0f,
			0.0f, 1.0f
	};

	GLuint el[] = {
			0, 1, 2, 0, 2, 3,
			4, 5, 6, 4, 6, 7,
			8, 9, 10, 8, 10, 11,
			12, 13, 14, 12, 14, 15,
			16, 17, 18, 16, 18, 19,
			20, 21, 22, 20, 22, 23
	};

	//boundingSphere = rev::Sphere::smallBall((glm::vec3*) v, 24);
	// boundingSphere = rev::Sphere::miniBall((glm::vec3*) v, 24);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
	glBufferData(GL_ARRAY_BUFFER, 24 * 3 * sizeof(float), v, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
	glBufferData(GL_ARRAY_BUFFER, 24 * 3 * sizeof(float), n, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[2]);
	glBufferData(GL_ARRAY_BUFFER, 24 * 2 * sizeof(float), tex, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 36 * sizeof(GLuint), el, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glAssert;

#if REV_ENGINE_MAX_CONTEXTS > 1
	resetVAOHAndles();
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}
