/*
 * RevVBOSphereVN.h
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 */

#ifndef REVVBOSPHEREVN_H_
#define REVVBOSPHEREVN_H_

#include <glm/glm.hpp>
#include "RevIDrawable.h"

namespace rev {

    class VBOSphereVN : public IDrawable {
        static const int HANDLE_SIZE = 3;
        static const int HANDLE_VERTEX = 0;
        static const int HANDLE_NORMAL = 1;
        static const int HANDLE_ELEMENT = 2;

        unsigned int handle[HANDLE_SIZE];
        int indices = 0;
        glm::vec3 center;
        float radius;
        int tessellation;

        int infoVertices = 0;
        int infoTriangles = 0;

        //VBOSphereVN();

    public:

        VBOSphereVN(float radius, int tessellation);
        VBOSphereVN(const glm::vec3& center, float radius, int tessellation);
        virtual ~VBOSphereVN();

        void render();

    private:
        bool generate();
        void initVAO(int contexIndex);
        void create(const glm::vec3& center, float radius, int tessellation);
    };

} /* namespace rev */
#endif /* REVVBOSPHEREVN_H_ */
