/*
 * RevVBOSphereVN.cpp
 *
 *  Created on: 14-12-2012
 *      Author: Revers
 */

#include <iostream>
#include <cmath>
#include <GL/glew.h>
#include <GL/gl.h>
#include <rev/gl/RevGLAssert.h>
#include <rev/common/RevAssert.h>
#include "RevVBOSphereVN.h"
#include "RevMathHelper.h"

#define PI 3.141592653589793
#define TWO_PI 6.2831853071795862
#define PI_OVER_TWO 1.5707963267948966
#define TO_RADIANS(x) (x * 0.017453292519943295)
#define TO_DEGREES(x) (x * 57.29577951308232)

using namespace rev;
using namespace glm;
using namespace std;

//VBOSphereVN::VBOSphereVN() {
//   glGenVertexArrays(1, &vaoHandle);
//  glGenBuffers(HANDLE_SIZE, handle);

//glAssert;
//}

VBOSphereVN::VBOSphereVN(float radius, int tessellation) {
	create(glm::vec3(0, 0, 0), radius, tessellation);
}

VBOSphereVN::VBOSphereVN(const glm::vec3& center,
		float radius, int tessellation) {
	create(center, radius, tessellation);
}

VBOSphereVN::~VBOSphereVN() {
	int count = REV_ENGINE_MAX_CONTEXTS;
	glDeleteBuffers(HANDLE_SIZE, handle);
}

void VBOSphereVN::create(const glm::vec3& center,
		float radius, int tessellation) {
	this->center = center;
	this->radius = radius;
	this->tessellation = tessellation;

	int count = REV_ENGINE_MAX_CONTEXTS;

	glGenBuffers(HANDLE_SIZE, handle);

	//boundingSphere = rev::Sphere(center, radius);
	clearVAOHandles(count);

	bool succ = generate();
	revAssert(succ);
}

bool VBOSphereVN::generate() {
	int verticalSegments = tessellation;
	int horizontalSegments = tessellation * 2;

	int size = 2 + (verticalSegments - 1) * horizontalSegments;

	vec3* v = new vec3[size];
	vec3* n = new vec3[size];

	infoVertices = size;
	infoTriangles = (verticalSegments - 2) * horizontalSegments * 2
			+ horizontalSegments * 2;
	//refreshBinder(&infoVertices);
	//refreshBinder(&infoTriangles);

	indices = infoTriangles * 3;
	unsigned int* el = new unsigned int[indices];
	int vIndex = 0;
	int nIndex = 0;
	int elIndex = 0;

	// Start with a single vertex at the bottom of the sphere.
	v[vIndex++] = MathHelper::DOWN * radius + center;
	n[nIndex++] = MathHelper::DOWN;

	// Create rings of vertices at progressively higher latitudes.
	for (int i = 0; i < verticalSegments - 1; i++) {
		float latitude = ((i + 1) * PI /
				verticalSegments) - PI_OVER_TWO;

		float dy = (float) sin(latitude);
		float dxz = (float) cos(latitude);

		// Create a single ring of vertices at this latitude.
		for (int j = 0; j < horizontalSegments; j++) {
			float longitude = j * 2 * PI / horizontalSegments;

			float dx = (float) cos(longitude) * dxz;
			float dz = (float) sin(longitude) * dxz;

			vec3 normal(dx, dy, dz);

			v[vIndex++] = normal * radius + center;
			n[nIndex++] = normal;
		}
	}

	// Finish with a single vertex at the top of the sphere.
	v[vIndex++] = MathHelper::UP * radius + center;
	n[nIndex++] = MathHelper::UP;

	// Create a fan connecting the bottom vertex to the bottom latitude ring.
	for (int i = 0; i < horizontalSegments; i++) {
		el[elIndex++] = 0;
		el[elIndex++] = 1 + (i + 1) % horizontalSegments;
		el[elIndex++] = 1 + i;
	}

	// Fill the sphere body with triangles joining each pair of latitude rings.
	for (int i = 0; i < verticalSegments - 2; i++) {
		for (int j = 0; j < horizontalSegments; j++) {
			int nextI = i + 1;
			int nextJ = (j + 1) % horizontalSegments;

			el[elIndex++] = 1 + i * horizontalSegments + j;
			el[elIndex++] = 1 + i * horizontalSegments + nextJ;
			el[elIndex++] = 1 + nextI * horizontalSegments + j;

			el[elIndex++] = 1 + i * horizontalSegments + nextJ;
			el[elIndex++] = 1 + nextI * horizontalSegments + nextJ;
			el[elIndex++] = 1 + nextI * horizontalSegments + j;
		}
	}

	// Create a fan connecting the top vertex to the top latitude ring.
	for (int i = 0; i < horizontalSegments; i++) {
		el[elIndex++] = size - 1;
		el[elIndex++] = size - 2 - (i + 1) % horizontalSegments;
		el[elIndex++] = size - 2 - i;
	}

	//boundingSphere = rev::Sphere(center, radius);

	// just in case I forgot to unbind some VAO:
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), v,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glBufferData(GL_ARRAY_BUFFER, 3 * size * sizeof(float), n,
	GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices * sizeof(unsigned int),
			el, GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glAssert;

	delete[] v;
	delete[] n;
	delete[] el;

#if REV_ENGINE_MAX_CONTEXTS > 1    
	resetVAOHAndles(REV_ENGINE_MAX_CONTEXTS);
#else
	initVAO(0);
#endif

	glAssert;
	return glGetError() == GL_NO_ERROR;
}

void VBOSphereVN::initVAO(int contextIndex) {
	if (vaoHandle[contextIndex] == 0) {
		glGenVertexArrays(1, &vaoHandle[contextIndex]);
	}

	glBindVertexArray(vaoHandle[contextIndex]);
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_VERTEX]);
	glVertexAttribPointer((GLuint) HANDLE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_VERTEX); // Vertex position
	//-----------------------------------------------------------------
	glBindBuffer(GL_ARRAY_BUFFER, handle[HANDLE_NORMAL]);
	glVertexAttribPointer((GLuint) HANDLE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0,
			((GLubyte *) NULL + (0)));
	glEnableVertexAttribArray(HANDLE_NORMAL); // Normal
	//-----------------------------------------------------------------
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, handle[HANDLE_ELEMENT]);
	//-----------------------------------------------------------------
	glBindVertexArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	vaoInited[contextIndex] = true;
}

void VBOSphereVN::render() {
#if REV_ENGINE_MAX_CONTEXTS > 1    
	int index = 0;
	if (!vaoInited[index]) {
		// lazy initialization
		initVAO(index);
	}
	glBindVertexArray(vaoHandle[index]);
#else
	glBindVertexArray(vaoHandle[0]);
#endif

	glDrawElements(GL_TRIANGLES, indices, GL_UNSIGNED_INT,
			((GLubyte *) NULL + (0)));
	// glAssert;
	glBindVertexArray(0);
}

