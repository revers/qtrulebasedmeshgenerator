/*
 * RevFreeCamera.cpp
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>

#include "RevFreeCamera.h"

using namespace rev;
using namespace glm;


void FreeCamera::move(const glm::vec3& translation) {
    //const float change = rev::Engine::ref().getCurrentFrameTime();
    position += vec3(rotation * vec4(translation, 0));
    calcViewMatrix();
}

void FreeCamera::click(int x, int y) {
    lastX = x;
    lastY = y;
}

void FreeCamera::drag(int x, int y) {
    int diffX = x - lastX;
    int diffY = lastY - y;
    lastX = x;
    lastY = y;

    const float change = 0.01f;//0.01 * rev::Engine::ref().getCurrentFrameTime();
    yaw -= change * diffX;
    pitch -= change * diffY;
    rotation = glm::yawPitchRoll(yaw, pitch, 0.0f);
    calcViewMatrix();
}

void FreeCamera::setScreenSize(float width, float height) {
    projectionMatrix = ICamera::createDefalutProjectionMatrix(width,
            height);
    calcViewMatrix();
}

void FreeCamera::calcViewMatrix() {
    direction = vec3(rotation * vec4(MathHelper::FORWARD, 0));
    glm::vec3 up = vec3(rotation * vec4(MathHelper::UP, 0));
    viewMatrix = glm::lookAt(position, position + direction, up);
}

