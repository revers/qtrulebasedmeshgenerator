/*
 * PhongShadingEffect.h
 *
 *  Created on: 28-11-2012
 *      Author: Revers
 */

#ifndef REVPHONGSHADINGEFFECT_H_
#define REVPHONGSHADINGEFFECT_H_

#include <glm/glm.hpp>
#include <rev/gl/RevGLSLProgram.h>

#include "RevICamera.h"
#include "RevIEffect.h"

#define PHONG_SHADING_EFFECT "Phong Shading"

namespace rev {

/**
 * This shader require object with following vertex attributes:
 * 1. position
 * 2. normal
 */
class PhongShadingEffect: public IEffect {
	glm::vec4 lightPosition;
	glm::vec3 lightColor;
	glm::vec3 diffuseColor; // Diffuse reflectivity
	glm::vec3 ambientColor; // Ambient reflectivity
	glm::vec3 specularColor; // Specular reflectivity
	float shininess; // Specular shininess factor

	int lightPositionLocation = -1;
	int lightColorLocation = -1;
	int diffuseColorLocation = -1;
	int ambientColorLocation = -1;
	int specularColorLocation = -1;
	int shininessLocation = -1;
	int modelViewMatrixLocation = -1;
	int normalMatrixLocation = -1;
	int mvpLocation = -1;

public:

	PhongShadingEffect(rev::GLSLProgram* program);
	PhongShadingEffect();

	virtual ~PhongShadingEffect() {
	}

	const char* getName() override {
		return PHONG_SHADING_EFFECT;
	}

	const glm::vec3& getAmbientColor() const {
		return ambientColor;
	}

	void setAmbientColor(const glm::vec3& ambientColor) {
		this->ambientColor = ambientColor;
	}

	const glm::vec3& getDiffuseColor() const {
		return diffuseColor;
	}

	void setDiffuseColor(const glm::vec3& diffuseColor) {
		this->diffuseColor = diffuseColor;
	}

	const glm::vec3& getLightColor() const {
		return lightColor;
	}

	void setLightColor(const glm::vec3& lightColor) {
		this->lightColor = lightColor;
	}

	const glm::vec4& getLightPosition() const {
		return lightPosition;
	}

	void setLightPosition(const glm::vec4& lightPosition) {
		this->lightPosition = lightPosition;
	}

	float getShininess() const {
		return shininess;
	}

	void setShininess(float shininess) {
		this->shininess = shininess;
	}

	const glm::vec3& getSpecularColor() const {
		return specularColor;
	}

	void setSpecularColor(const glm::vec3& specularColor) {
		this->specularColor = specularColor;
	}

	virtual void apply(
			const glm::mat4& modelMatrix,
			const glm::mat4& viewMatrix,
			const glm::mat4& projectionMatrix,
			const ICamera& camera);

	void applyLightParameters();

	static rev::GLSLProgram* createGLSLProgram();

private:
	void setDefaultValues();

};

} /* namespace rev */
#endif /* REVPHONGSHADINGEFFECT_H_ */
