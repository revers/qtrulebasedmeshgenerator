#ifndef REVVBOCUBEVNT_H
#define REVVBOCUBEVNT_H

#include <glm/glm.hpp>
#include "RevEngineConfig.h"
#include "RevIDrawable.h"

namespace rev {

class VBOCubeVNT: public IDrawable {
private:
	unsigned int handle[4];
	float side = 1;

	int infoVertices = 24;
	int infoTriangles = 12;

public:

	VBOCubeVNT(float side);
	~VBOCubeVNT();

	void render() override;

private:
	bool generate();

	void initVAO(int contextIndex);

};

}
#endif // REVVBOCUBEVNT_H
