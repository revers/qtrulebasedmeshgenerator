/* 
 * File:   RevIDrawable.h
 * Author: Revers
 *
 * Created on 2 luty 2013, 11:08
 */

#ifndef REVIDRAWABLE_H
#define	REVIDRAWABLE_H

#include "RevEngineConfig.h"
#include <GL/glew.h>

namespace rev {

class IDrawable {
protected:
	unsigned int vaoHandle[REV_ENGINE_MAX_CONTEXTS];
	bool vaoInited[REV_ENGINE_MAX_CONTEXTS];

public:
	virtual ~IDrawable() {
		glDeleteVertexArrays(REV_ENGINE_MAX_CONTEXTS, &vaoHandle[0]);
			//  glAssert;
	}

	virtual void render() = 0;

protected:

	void clearVAOHandles(int count) {
		for (int i = 0; i < count; i++) {
			vaoHandle[i] = 0;
			vaoInited[i] = false;
		}
	}

	void resetVAOHAndles(int count) {
		for (int i = 0; i < count; i++) {
			vaoInited[i] = false;
		}
	}
};
}

#endif	/* REVIDRAWABLE_H */

