/*
 * PhongShadingEffect.cpp
 *
 *  Created on: 28-11-2012
 *      Author: Revers
 */

#include <rev/common/RevErrorStream.h>
#include <rev/common/RevResourcePath.h>
#include <rev/gl/RevGLAssert.h>
#include "RevPhongShadingEffect.h"
#include <rev/common/RevAssert.h>

using namespace glm;
using namespace rev;

#define PHONG_SHADING_SHADER_FILE "shaders/PhongShading.glsl"

PhongShadingEffect::PhongShadingEffect(rev::GLSLProgram* program) :
		IEffect(program, false) {
	setDefaultValues();
}

PhongShadingEffect::PhongShadingEffect() :
		IEffect(createGLSLProgram(), true) {
	setDefaultValues();
}

void PhongShadingEffect::applyLightParameters() {
	program->use();
	setProperty(ambientColorLocation, ambientColor);
	setProperty(diffuseColorLocation, diffuseColor);
	setProperty(specularColorLocation, specularColor);
	setProperty(lightColorLocation, lightColor);
	setProperty(shininessLocation, shininess);
}

void PhongShadingEffect::apply(
		const glm::mat4& modelMatrix,
		const glm::mat4& viewMatrix,
		const glm::mat4& projectionMatrix,
		const ICamera& camera) {
	program->use();

	setProperty(lightPositionLocation, viewMatrix * lightPosition);
	mat4 mv = camera.getViewMatrix() * modelMatrix;
	setProperty(modelViewMatrixLocation, mv);
	setProperty(normalMatrixLocation, mat3(mv));
	setProperty(mvpLocation, projectionMatrix * mv);
}

void PhongShadingEffect::setDefaultValues() {
	lightPosition = glm::vec4(100.0f, 100.0f, 100.0f, 1.0f);
	lightColor = glm::vec3(1.0f, 1.0f, 1.0f);
	diffuseColor = glm::vec3(0.9f, 0.9f, 0.9f);
	specularColor = glm::vec3(0.95f, 0.95f, 0.95f);
	ambientColor = glm::vec3(0.1f, 0.1f, 0.1f);
	shininess = 100.0f;

	program->use();

	lightPositionLocation = program->getUniformLocation("LightPosition");
	lightColorLocation = program->getUniformLocation("LightColor");
	diffuseColorLocation = program->getUniformLocation("Kd");
	ambientColorLocation = program->getUniformLocation("Ka");
	specularColorLocation = program->getUniformLocation("Ks");
	shininessLocation = program->getUniformLocation("Shininess");

	modelViewMatrixLocation = program->getUniformLocation("ModelViewMatrix");
	normalMatrixLocation = program->getUniformLocation("NormalMatrix");
	mvpLocation = program->getUniformLocation("MVP");

	revAssert(lightPositionLocation >= 0
			&& lightColorLocation >= 0
			&& diffuseColorLocation >= 0
			&& ambientColorLocation >= 0
			&& specularColorLocation >= 0
			&& shininessLocation >= 0
			&& modelViewMatrixLocation >= 0
			&& normalMatrixLocation >= 0
			&& mvpLocation >= 0);

	applyLightParameters();
}

GLSLProgram* PhongShadingEffect::createGLSLProgram() {
	REV_TRACE_FUNCTION;
	GLSLProgram* program = new GLSLProgram();

	std::string path = ResourcePath::get(PHONG_SHADING_SHADER_FILE);
	if (!program->compileShaderGLSLFile(path.c_str())) {
		REV_ERROR_MSG("Compilation of file " << path << " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	glBindAttribLocation(program->getHandle(), 0, "VertexPosition");
	glBindAttribLocation(program->getHandle(), 1, "VertexNormal");
	glBindAttribLocation(program->getHandle(), 0, "FragColor");
	glAssert;

	if (!program->link()) {
		REV_ERROR_MSG("Linking file " PHONG_SHADING_SHADER_FILE " FAILED!!");
		delete program;
		assert(false);
		return nullptr;
	}

	return program;
}
