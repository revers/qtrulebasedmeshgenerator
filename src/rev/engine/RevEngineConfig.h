/*
 * RevEngineConfig.h
 *
 *  Created on: 29-12-2012
 *      Author: Revers
 */

#ifndef REVENGINECONFIG_H_
#define REVENGINECONFIG_H_

#define REV_ENGINE_QT
//#define REV_ENGINE_GLFW

#define REV_ENGINE_MULTIPLE_CONTEXTS
#define REV_ENGINE_MAX_CONTEXTS 1

#if defined(WIN32) || defined(_WIN32) || defined(__WIN32__) || defined(_WIN64)
#define REV_ENGINE_WINDOWS
#endif

#endif /* REVENGINECONFIG_H_ */
