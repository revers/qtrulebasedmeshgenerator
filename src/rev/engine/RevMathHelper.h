/*
 * RevMathHelper.h
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#ifndef REVMATHHELPER_H_
#define REVMATHHELPER_H_

#include <cmath>
#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>

#define EPSILON_F 0.00001f
#define EPSILON_D 0.0000000001

namespace rev {

    inline bool almostEqual(float x, float y) {
        return fabs(x - y) < EPSILON_F;
    }
    inline bool almostEqual(double x, double y) {
        return fabs(x - y) < EPSILON_D;
    }

    class MathHelper {
        MathHelper() {
        }
        ~MathHelper() {
        }

    public:
        static const glm::vec3 FORWARD;
        static const glm::vec3 BACK;
        static const glm::vec3 UP;
        static const glm::vec3 DOWN;
        static const glm::vec3 RIGHT;
        static const glm::vec3 LEFT;

        static const glm::vec3 ZERO;
        static const glm::vec3 ONE;

        static glm::vec3 anyOrthogonal(const glm::vec3& v) {
            if (!almostEqual(v.x, v.y) || !almostEqual(v.y, v.z)) {
                // simple componenent permutation
                return glm::cross(v, glm::vec3(v.z, v.x, v.y));
            } else {
                // cross product with versor
                return glm::cross(v, glm::vec3(1.0f, 0.0f, 0.0f));
            }
        }

        /**
         * Decomposes glm::perspective() matrix.
         *
         * @param p      [in]  a glm::perspective() matrix.
         * @param fovy   [out] angle
         * @param aspect [out] aspect ratio (width / height)
         * @param near_  [out] near plane
         * @param far_   [out] far plane
         */
        static void decomposePerspective(const glm::mat4& p,
                float& fovy, float& aspect, float& near_, float& far_);

        /**
         * Decomposes matrix M such that T * R * S = M, where T is translation matrix,
         * R is rotation matrix and S is scaling matrix.
         * http://code.google.com/p/assimp-net/source/browse/trunk/AssimpNet/Matrix4x4.cs
         * (this method is exact to at least 0.0001f)
         *
         * | 1  0  0  T1 | | R11 R12 R13 0 | | a 0 0 0 |   | aR11 bR12 cR13 T1 |
         * | 0  1  0  T2 |.| R21 R22 R23 0 |.| 0 b 0 0 | = | aR21 bR22 cR23 T2 |
         * | 0  0  0  T3 | | R31 R32 R33 0 | | 0 0 c 0 |   | aR31 bR32 cR33 T3 |
         * | 0  0  0   1 | |  0   0   0  1 | | 0 0 0 1 |   |  0    0    0    1 |
         *
         * @param m (in) matrix to decompose
         * @param scaling (out) scaling vector
         * @param rotation (out) rotation matrix
         * @param translation (out) translation vector
         */
        static void decomposeTRS(const glm::mat4& m, glm::vec3& scaling,
                glm::mat4& rotation, glm::vec3& translation);

        /**
         * Decompose matrix M, such that M = QR, where Q is an orthogonal matrix, and R is some matrix.
         * http://www.keithlantz.net/2012/05/qr-decomposition-using-householder-transformations/
         *
         * @param M matrix to decompose.
         * @param Q out parameter
         * @param R out parameter
         */
        static void decomposeQR(const glm::mat4& M, glm::mat4& Q, glm::mat4& R);

        /**
         * Decompose matrix M, such that M = RQ, where Q is an orthogonal matrix, and R is some matrix.
         * http://www.keithlantz.net/2012/05/qr-decomposition-using-householder-transformations/
         *
         * @param M matrix to decompose.
         * @param Q out parameter
         * @param R out parameter
         */
        static void decomposeRQ(const glm::mat4& M, glm::mat4& Q, glm::mat4& R);

    private:
        static glm::mat4 multTranspose(const glm::vec4& A,
                const glm::vec4& B);

    };

} /* namespace rev */
#endif /* REVMATHHELPER_H_ */

