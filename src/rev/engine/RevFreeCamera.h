/*
 * RevFreeCamera.h
 *
 *  Created on: 12-12-2012
 *      Author: Revers
 */

#ifndef REVFREECAMERA_H_
#define REVFREECAMERA_H_

#include <glm/glm.hpp>
#include "RevMathHelper.h"
#include "RevICamera.h"

namespace rev {

    class FreeCamera: public ICamera {
    	static const glm::vec3 FORWARD;
    	        static const glm::vec3 BACK;
    	        static const glm::vec3 UP;
    	        static const glm::vec3 DOWN;
    	        static const glm::vec3 RIGHT;
    	        static const glm::vec3 LEFT;

        glm::vec3 position;
        glm::vec3 direction;
        glm::mat4 viewMatrix;
        glm::mat4 projectionMatrix;
        glm::mat4 rotation;
        float yaw = 0;
        float pitch = 0;
        int lastX = 0;
        int lastY = 0;

    public:
        FreeCamera() :
                position(0.0f), direction(MathHelper::FORWARD) {
        }

        FreeCamera(const glm::vec3& position) :
                position(position), direction(MathHelper::FORWARD) {
        }

        FreeCamera(const glm::vec3& position, const glm::vec3& direction) :
                position(position), direction(direction) {
        }

        virtual ~FreeCamera() {
        }

        const glm::vec3& getPosition() const {
            return position;
        }
        const glm::vec3& getDirection() const {
            return direction;
        }
        const glm::mat4& getViewMatrix() const {
            return viewMatrix;
        }
        const glm::mat4& getProjectionMatrix() const {
            return projectionMatrix;
        }

        void move(float translationX, float translationY,
                float translationZ) {
            move(glm::vec3(translationX, translationY, translationZ));
        }
        void move(const glm::vec3& translation);
        void click(int x, int y);
        void drag(int x, int y);
        void setScreenSize(float width, float height);

    private:
        void calcViewMatrix();
    };

} /* namespace rev */
#endif /* REVFREECAMERA_H_ */
