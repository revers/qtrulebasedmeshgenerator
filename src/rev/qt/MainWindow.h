/*
 * MainWindow.h
 *
 *  Created on: 2013-08-09, 20:36:23
 *      Author: Revers
 */

#ifndef _MAINWINDOW_H
#define	_MAINWINDOW_H

#include "ui_MainWindow.h"
#include <rev/common/RevFPSCounter.hpp>

class QGLFormat;

namespace rev {

class GLWidget;

class MainWindow: public QMainWindow {
Q_OBJECT

private:
	static MainWindow* mainWindow;
	rev::GLWidget* glWidget = nullptr;
	rev::FPSCounter fpsCounter;

public:
	Ui::MainWindow widget;

	MainWindow(const QGLFormat& format);
	virtual ~MainWindow();

	static MainWindow& getInstance() {
		return *mainWindow;
	}

	rev::GLWidget* getGLWidget() {
		return glWidget;
	}

	void init();

private:
	void setupActions();

private slots:
	void aboutAction();
	void openAction();
	void updateGLWidget();
};

} /* namespace rev */
#endif	/* _MAINWINDOW_H */
