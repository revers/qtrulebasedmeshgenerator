/*
 * MainWindow.cpp
 *
 *  Created on: 2013-08-09, 20:36:23
 *      Author: Revers
 */

#include <QTimer>
#include <QHBoxLayout>
#include <QMessageBox>
#include <QSettings>
#include <QFileDialog>

#include "MainWindow.h"
#include "GLWidget.h"

#ifdef NDEBUG
#define WINDOW_TITLE "Rule Based Mesh Generator"
#elif defined(REV_DEBUG_FULL)
#define WINDOW_TITLE "Rule Based Mesh Generator (DEBUG_FULL)"
#else
#define WINDOW_TITLE "Rule Based Mesh Generator (DEBUG)"
#endif

namespace rev {

MainWindow* MainWindow::mainWindow = nullptr;

static const QString DEFAULT_DIR_KEY("default_dir");

MainWindow::MainWindow(const QGLFormat& format) {
	mainWindow = this;

	widget.setupUi(this);
	glWidget = new GLWidget(format, this);
	QHBoxLayout* layout = (QHBoxLayout*) (widget.centralwidget->layout());
	layout->addWidget(glWidget);

	setupActions();

	QTimer* engineTimer = new QTimer(this);
	QObject::connect(engineTimer, SIGNAL(timeout()), this, SLOT(updateGLWidget()));
	engineTimer->start(0);
}

MainWindow::~MainWindow() {
}

void MainWindow::updateGLWidget() {
	fpsCounter.frameBegin();

	glWidget->repaint();

	if (fpsCounter.frameEnd()) {
		QString title;
		float fps = fpsCounter.getFramesPerSec();
		setWindowTitle(
				title.sprintf(WINDOW_TITLE " | %.2f", fps));
	}
}

void MainWindow::setupActions() {
	connect(widget.actionExit, SIGNAL(triggered(bool)), qApp, SLOT(quit()));

	connect(widget.actionAbout, SIGNAL(triggered(bool)),
			this, SLOT(aboutAction()));

	connect(widget.actionOpen, SIGNAL(triggered(bool)),
			this, SLOT(openAction()));
}

void MainWindow::aboutAction() {
	QMessageBox::information(this, "About...", "Author: Kamil Kolaczynski");
}

void MainWindow::openAction() {
	QSettings mySettings;
	QString filename = QFileDialog::getOpenFileName(
			this,
			tr("Open Scene"),
			mySettings.value(DEFAULT_DIR_KEY).toString(),
			tr("Scene files (*.xml)"));
	if (filename.isNull()) {
		return;
	}

	QDir currentDir;
	mySettings.setValue(DEFAULT_DIR_KEY, currentDir.absoluteFilePath(filename));

	if (!glWidget->loadScene(filename.toUtf8().data())) {
		QMessageBox::critical(this, "Parsing Error",
				QString("There are some errors in the file:\n%1")
						.arg(filename));
	}
}

void MainWindow::init() {
	glWidget->loadScene("rules/Five.xml");
}

} /* namespace rev */
