/*
 * GLWidget.h
 *
 *  Created on: 9 sie 2013
 *      Author: Revers
 */

#ifndef GLWIDGET_H_
#define GLWIDGET_H_

#include <GL/glew.h>
#include <QGLWidget>

#include "../engine/RevFreeCamera.h"
#include "../rule/RuleRenderer.h"

class QMouseEvent;
class QWheelEvent;

namespace rev {

class MainWindow;

class GLWidget: public QGLWidget {
Q_OBJECT

private:
	enum Shape {
		CUBE, SPHERE, CYLINDER
	};

	bool mousePressed = false;
	MainWindow* mainWindow;
	FreeCamera camera;
	RuleRenderer renderer;

public:
	GLWidget(const QGLFormat& format, MainWindow* parent);

	bool loadScene(const char* filename);

	FreeCamera& getCamera() {
		return camera;
	}

protected:
	void mousePressEvent(QMouseEvent* event) override;
	void mouseReleaseEvent(QMouseEvent* event) override;
	void mouseMoveEvent(QMouseEvent* event) override;
	void wheelEvent(QWheelEvent* event) override;
	void keyPressEvent(QKeyEvent* e) override;
	void initializeGL() override;
	void paintGL() override;
	void resizeGL(int w, int h) override;

	virtual ~GLWidget();

private slots:
	void refreshAction();
	void shapeIndexChanged(int index);
	void sizeValueChanged(int value);

private:
	void setupActions();
	void setObjectsCount();
};

} /* namespace rev */
#endif /* GLWIDGET_H_ */
