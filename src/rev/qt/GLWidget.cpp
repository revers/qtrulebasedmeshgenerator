/*
 * GLWidget.cpp
 *
 *  Created on: 9 sie 2013
 *      Author: Revers
 */

#include <iostream>
#include <glm/glm.hpp>

#include <QMouseEvent>
#include <QWheelEvent>
#include <QKeyEvent>

#include <rev/common/RevAssert.h>
#include <rev/common/RevErrorStream.h>
#include <rev/gl/RevGLAssert.h>

#include "MainWindow.h"
#include "GLWidget.h"

namespace rev {

static const float CLEAR_COLOR_R = 203.0 / 255.0;
static const float CLEAR_COLOR_G = 214.0 / 255.0;
static const float CLEAR_COLOR_B = 230.0 / 255.0;

GLWidget::GLWidget(const QGLFormat& format, MainWindow* parent) :
		QGLWidget(format, parent), mainWindow(parent) {
	// This tells the widget to accept keyboard
	// focus when the widget is clicked.
	this->setFocusPolicy(Qt::ClickFocus);

	Ui::MainWindow& ui = mainWindow->widget;
	ui.shapeCB->addItem("Cube");
	ui.shapeCB->addItem("Sphere");
	ui.shapeCB->addItem("Cylinder");

	setupActions();
}

GLWidget::~GLWidget() {
}

void GLWidget::initializeGL() {
	GLenum err = glewInit();
	if (GLEW_OK != err) {
		REV_ERROR_MSG("Error initializing GLEW: "
				<< glewGetErrorString(err));
		exit(-1);
	}

	glAlwaysAssertNoExit;
	REV_INFO_MSG("GLEW inited successfully :)");

	glEnable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glAssert;

	glClearColor(CLEAR_COLOR_R, CLEAR_COLOR_G, CLEAR_COLOR_B, 1.0);

	camera.move(0, 0, -5);
	glAssert;

	renderer.init();
}

void GLWidget::paintGL() {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	renderer.render();
}

void GLWidget::resizeGL(int w, int h) {
	camera.setScreenSize(w, h);
	glViewport(0, 0, w, h);
}

void GLWidget::mousePressEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		mousePressed = true;
		camera.click(event->x(), event->y());
	}

	repaint();
}

void GLWidget::mouseReleaseEvent(QMouseEvent* event) {
	if (event->button() == Qt::LeftButton) {
		mousePressed = false;
	}
}

void GLWidget::mouseMoveEvent(QMouseEvent* event) {
	if (!mousePressed) {
		return;
	}

	camera.drag(event->x(), event->y());
	const glm::vec3& pos = camera.getPosition();
	const glm::vec3& dir = camera.getDirection();

	repaint();
}

void GLWidget::wheelEvent(QWheelEvent* event) {
	repaint();
}

void GLWidget::keyPressEvent(QKeyEvent* e) {
	float step = 0.5;

	if (e->key() == Qt::Key_Up) {
		camera.move(0, 0, step);
	} else if (e->key() == Qt::Key_Down) {
		camera.move(0, 0, -step);
	} else if (e->key() == Qt::Key_Right) {
		camera.move(-step, 0, 0);
	} else if (e->key() == Qt::Key_Left) {
		camera.move(step, 0, 0);
	} else if (e->key() == Qt::Key_F5) {
		renderer.reloadScene();
		setObjectsCount();
	}

	repaint();
}

void GLWidget::refreshAction() {
	renderer.reloadScene();
	setObjectsCount();
}

void GLWidget::shapeIndexChanged(int index) {
	switch (index) {
	case CUBE:
		renderer.useCubes();
		break;
	case SPHERE:
		renderer.useSpheres();
		break;
	case CYLINDER:
		renderer.useCylinders();
		break;
	}
}

void GLWidget::sizeValueChanged(int value) {
	float percent;
	const float multiplier = 10.0f;

	if (value <= 50) {
		value = 50 - value;
		float val = float(value) / 100.0f;
		percent = 1.0f / (1.0f + val * multiplier);
	} else {
		value -= 50;
		float val = float(value) / 100.0f;
		percent = 1.0f + val * multiplier;
	}
	renderer.setScale(percent);
}

void GLWidget::setupActions() {
	Ui::MainWindow& ui = mainWindow->widget;
	connect(ui.refreshB, SIGNAL(clicked()),
			this, SLOT(refreshAction()));

	connect(ui.shapeCB, SIGNAL(currentIndexChanged(int)),
			this, SLOT(shapeIndexChanged(int)));

	connect(ui.sizeSlider, SIGNAL(valueChanged(int)),
			this, SLOT(sizeValueChanged(int)));
}

void GLWidget::setObjectsCount() {
	Ui::MainWindow& ui = mainWindow->widget;
	ui.objectsL->setText(QString::number(renderer.getObjectsCount()));
}

bool GLWidget::loadScene(const char* filename) {
	bool succ = renderer.loadScene(filename);
	if (succ) {
		setObjectsCount();
	}
	return succ;
}

} /* namespace rev */
