/*
 * RuleRenderer.h
 *
 *  Created on: 10 sie 2013
 *      Author: Revers
 */

#ifndef RULERENDERER_H_
#define RULERENDERER_H_

#include <random>
#include <vector>
#include "../engine/RevFreeCamera.h"
#include "../engine/RevIEffect.h"
#include "../engine/RevIDrawable.h"

namespace rev {

class XMLRule;
class XMLRules;
class VBOCubeVNT;
class VBOSphereVN;
class VBOCylinderVN;

class RuleRenderer {
	const int SEED = 12345;

	std::mt19937 randEngine = std::mt19937(SEED);
	std::vector<glm::mat4> matrices;
	rev::XMLRules* rules = nullptr;
	rev::VBOCubeVNT* cube = nullptr;
	rev::VBOSphereVN* sphere = nullptr;
	rev::VBOCylinderVN* cylinder = nullptr;
	rev::IDrawable* shape = nullptr;
	IEffect* effect = nullptr;
	std::string sceneFile = std::string("rules/Five.xml");
	glm::mat4 scale;

public:
	RuleRenderer();
	~RuleRenderer();

	bool loadScene(const char* filename);
	void reloadScene() {
		loadScene(sceneFile.c_str());
	}
	void render();
	void init();

	int getObjectsCount() {
		return matrices.size();
	}

	void useCubes();
	void useSpheres();
	void useCylinders();
	void setScale(float s);

private:
	XMLRule* pickRule(const char* name);
	void processRule(XMLRule* rule, int depth, const glm::mat4& matrix = glm::mat4(1.0f));
	void generateMatrices();

	int random(int min, int max);
};


} /* namespace rev */
#endif /* RULERENDERER_H_ */
