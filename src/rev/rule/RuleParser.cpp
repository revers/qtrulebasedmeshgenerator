/*
 * RuleParser.cpp
 *
 *  Created on: 14 lip 2013
 *      Author: Revers
 */

#include <iostream>

#include <rev/common/RevResourcePath.h>
#include <rev/common/RevErrorStream.h>
#include "../xml/XMLRules.h"
#include "../xml/XMLRule.h"
#include "../xml/XMLCall.h"
#include "../xml/XMLInstance.h"

#include "Transforms.h"
#include "RuleParser.h"

using namespace std;

namespace rev {

XMLRules* RuleParser::parse(const char* fileName) {
	std::string path = ResourcePath::get(fileName);
	REV_TRACE_MSG("Parsing file " << path);

	XMLRules* rules = new XMLRules();
	rules->loadFromFile(path.c_str());

	for (XMLRule*& rule : rules->getRuleList()) {
		for (XMLCall*& call : rule->getCallList()) {
			REV_DEBUG_MSG("Parsing " << *call);

			Transforms trans;
			if (!trans.parse(call->getTransformsText())) {
				goto error;
			}

			call->setMatrix(trans.getMatrix());
		}

		for (XMLInstance*& instance : rule->getInstanceList()) {
			REV_DEBUG_MSG("Parsing " << *instance);

			const std::string& shape = instance->getShape();
			if (shape == "sphere") {
				instance->setInstanceShape(InstanceShape::SPHERE);
			} else if (shape == "box") {
				instance->setInstanceShape(InstanceShape::BOX);
			} else if (shape == "tube") {
				instance->setInstanceShape(InstanceShape::TUBE);
			} else {
				REV_ERROR_MSG("Unknown shape: '" << shape << "' for instance: " << *instance);
				goto error;
			}

			Transforms trans;
			if (!trans.parse(instance->getTransformsText())) {
				goto error;
			}

			instance->setMatrix(trans.getMatrix());
		}
	}

	return rules;

	error: delete rules;
	return nullptr;
}

} /* namespace rev */
