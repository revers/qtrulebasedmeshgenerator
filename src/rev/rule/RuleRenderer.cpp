/*
 * RuleRenderer.cpp
 *
 *  Created on: 10 sie 2013
 *      Author: Revers
 */

#include <iostream>
#include <vector>
#include <glm/gtx/transform.hpp>
#include <rev/gl/RevGLAssert.h>
#include "../xml/XMLRules.h"
#include "../engine/RevPhongShadingEffect.h"
#include "../engine/RevVBOCubeVNT.h"
#include "../engine/RevVBOSphereVN.h"
#include "../engine/RevVBOCylinderVN.h"

#include "../qt/MainWindow.h"
#include "../qt/GLWidget.h"

#include "RuleParser.h"
#include "RuleRenderer.h"

using namespace glm;
using namespace std;

namespace rev {

RuleRenderer::RuleRenderer() {
}

RuleRenderer::~RuleRenderer() {
	if (rules) {
		delete rules;
	}
	if (cube) {
		delete cube;
	}
	if (sphere) {
		delete sphere;
	}
	if (cylinder) {
		delete cylinder;
	}
	if (effect) {
		delete effect;
	}
}

bool RuleRenderer::loadScene(const char* filename) {
	REV_TRACE_MSG("Loading scene: " << filename);
	sceneFile = filename;

	if (rules) {
		delete rules;
	}
	rules = RuleParser::parse(filename);
	if (!rules) {
		REV_ERROR_MSG("Rules initialization failed! :(");
		return false;
	}
	REV_INFO_MSG("Rules initialized successfully :)");
	REV_DEBUG_MSG(*rules);

	generateMatrices();

	return true;
}

void RuleRenderer::init() {
	REV_TRACE_FUNCTION;
	if (!cube) {
		cube = new VBOCubeVNT(1.0f);
		//cube = new VBOCubeVNT(0.1f);
	}
	if (!sphere) {
		sphere = new VBOSphereVN(1.0f, 20);
	}
	if (!cylinder) {
		//cylinder = new VBOCylinderVN(0.3f, 0.15f, 20);
		cylinder = new VBOCylinderVN(2.0f, 1.0f, 20);
	}
	useCubes();

	effect = new PhongShadingEffect();
	glAssert;
}

#define M(x, y) m[(x)][(y)] << ", "

template<typename T>
inline std::ostream& operator<<(std::ostream& out, const glm::detail::tmat4x4<T>& m) {
	out
			<< M(0, 0)<< M(1, 0) << M(2, 0) << M(3, 0) << '\n'
	<< M(0, 1) << M(1, 1) << M(2, 1) << M(3, 1) << '\n'
	<< M(0, 2) << M(1, 2) << M(2, 2) << M(3, 2) << '\n'
	<< M(0, 3) << M(1, 3) << M(2, 3) << M(3, 3) << '\n';
	return out;
}

void RuleRenderer::generateMatrices() {
	matrices.clear();

	XMLRule* entry = pickRule("entry");
	processRule(entry, rules->getMaxDepth());

	REV_WARN_MSG("matrices.size() = " << matrices.size());
	// cout << matrices[0] << endl;
}

void RuleRenderer::processRule(XMLRule* rule, int depth, const glm::mat4& matrix) {
	if (depth < 1) {
		return;
	}

	mat4 m = matrix;

	for (XMLCall*& call : rule->getCallList()) {
		for (int i = 0; i < call->getCount(); i++) {
			m *= call->getMatrix();
			XMLRule* r = pickRule(call->getRule().c_str());
			processRule(r, depth - 1, m);
		}
	}

	for (XMLInstance*& instance : rule->getInstanceList()) {
		m *= instance->getMatrix();
		matrices.push_back(m);
	}
}

XMLRule* RuleRenderer::pickRule(const char* name) {
	int sum = 0;
	std::vector<XMLRule*> ruleVector;
	for (XMLRule*& e : rules->getRuleList()) {
		if (e->getName() == name) {
			ruleVector.push_back(e);
			sum += e->getWeight();
		}
	}

	int n = random(0, sum - 1);

	for (XMLRule*& e : ruleVector) {
		if (n < e->getWeight()) {
			return e;
		}
		n -= e->getWeight();
	}

	//return ruleVector[0];
	revAssertMsg(false, "Cannot find rule with name: '" << name << "'");
	return nullptr;
}

int RuleRenderer::random(int min, int max) {
	std::uniform_int_distribution<int> distr(min, max);
	return distr(randEngine);
}

void RuleRenderer::useCubes() {
	shape = cube;
}

void RuleRenderer::useSpheres() {
	shape = sphere;
}

void RuleRenderer::useCylinders() {
	shape = cylinder;
}

void RuleRenderer::setScale(float s) {
	scale = glm::scale(s, s, s);
}

void RuleRenderer::render() {
	GLWidget* glWidget = MainWindow::getInstance().getGLWidget();
	FreeCamera& camera = glWidget->getCamera();

	for (mat4& m : matrices) {
		effect->apply(m * scale,
				camera.getViewMatrix(),
				camera.getProjectionMatrix(),
				camera);

		shape->render();
	}
}

} /* namespace rev */
