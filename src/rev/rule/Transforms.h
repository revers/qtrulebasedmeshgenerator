/*
 * Transforms.h
 *
 *  Created on: 13 lip 2013
 *      Author: Revers
 */

#ifndef TRANSFORMS_H_
#define TRANSFORMS_H_

#include <vector>

namespace rev {

class Transforms {
public:
	typedef std::vector<std::string> StringVector;
	typedef StringVector::iterator StringVectorIter;
	private:

	glm::mat4 matrix;

public:
	Transforms() {
	}

	~Transforms() {
	}

	bool parse(const std::string& code);

	const glm::mat4& getMatrix() const {
		return matrix;
	}

private:
	bool parseRotation(StringVectorIter& begin, StringVectorIter& end);
	bool parseTranslation(StringVectorIter& begin, StringVectorIter& end);
	bool parseScaling(StringVectorIter& begin, StringVectorIter& end);

	bool toFloat(const std::string& str, float& result);
	bool getNumbers(StringVectorIter& begin, StringVectorIter& end, int count, float* result);
};

} /* namespace rev */
#endif /* TRANSFORMS_H_ */
