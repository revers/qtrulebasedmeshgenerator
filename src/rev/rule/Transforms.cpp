/*
 * Transforms.cpp
 *
 *  Created on: 13 lip 2013
 *      Author: Revers
 */

#include <iostream>
#include <sstream>
#include <glm/gtx/transform.hpp>
#include <glm/gtx/euler_angles.hpp>
#include <rev/common/RevErrorStream.h>
#include <rev/common/RevAssert.h>
#include <rev/common/RevStringTokenizer.h>
#include "Transforms.h"

using namespace glm;
using namespace std;

namespace rev {

#define M(x, y) m[(x)][(y)] << ", "

template<typename T>
inline std::ostream& operator<<(std::ostream& out, const glm::detail::tmat4x4<T>& m) {
	out
			<< M(0, 0)<< M(1, 0) << M(2, 0) << M(3, 0) << '\n'
	<< M(0, 1) << M(1, 1) << M(2, 1) << M(3, 1) << '\n'
	<< M(0, 2) << M(1, 2) << M(2, 2) << M(3, 2) << '\n'
	<< M(0, 3) << M(1, 3) << M(2, 3) << M(3, 3) << '\n';
	return out;
}

bool Transforms::parse(const std::string& code) {
	REV_DEBUG_MSG("Parsing: \"" << code << "\"");

	matrix = mat4(1.0f);

	if (code.empty()) {
		return true;
	}
	StringVectorPtr tokensPtr = StringTokenizer::splitByCharSet(code);
	StringVectorIter begin = tokensPtr->begin();
	StringVectorIter end = tokensPtr->end();

	while (begin != end) {
		string& token = *begin;
		if (token.size() > 2) {
			REV_ERROR_MSG("Command length is too long: " << token.size() << " (Expected <= 2)");
			return false;
		}
		char firstLetter = token[0];
		bool succ = true;

		if (firstLetter == 'r') {
			succ = parseRotation(begin, end);
		} else if (firstLetter == 't') {
			succ = parseTranslation(begin, end);
		} else if (firstLetter == 's') {
			succ = parseScaling(begin, end);
		} else {
			REV_ERROR_MSG("Unknown command: '" << token << "'");
			return false;
		}

		if (!succ) {
			return false;
		}
	}

	//cout << matrix << endl;

	return true;
}

bool Transforms::getNumbers(StringVectorIter& begin, StringVectorIter& end, int count,
		float* result) {
	int n = 0;
	while (n < count) {
		if (begin == end) {
			REV_ERROR_MSG("Wrong number of arguments: " << n << ". Expected: " << count);
			return false;
		}

		float value;
		std::string& str = *begin++;
		if (!toFloat(str, value)) {
			REV_ERROR_MSG("Argument #" << (n + 1) << " is not a number: '" << str << "'");
			return false;
		}
		result[n++] = value;
	}
	return true;
}

bool Transforms::parseRotation(StringVectorIter& begin, StringVectorIter& end) {
	std::string& name = *begin++;

	float values[3] = { 0.0f, 0.0f, 0.0f };
	bool valid = true;
	if (name == "r") {
		valid = getNumbers(begin, end, 3, values);
		//matrix *= glm::yawPitchRoll(radians(values[1]), radians(values[0]), radians(values[2]));
	} else if (name == "rx") {
		valid = getNumbers(begin, end, 1, values);
		//matrix *= glm::rotate(values[0], 1.0f, 0.0f, 0.0f);
	} else if (name == "ry") {
		valid = getNumbers(begin, end, 1, values + 1);
		//matrix *= glm::rotate(values[1], 0.0f, 1.0f, 0.0f);
	} else if (name == "rz") {
		valid = getNumbers(begin, end, 1, values + 2);
		//matrix *= glm::rotate(-values[2], 0.0f, 0.0f, 1.0f);
	} else {
		REV_ERROR_MSG("Unknown command '" << name << "'!");
		return false;
	}

	if (!valid) {
		REV_ERROR_MSG("Command '" << name << "' is invalid!");
		return false;
	}

	REV_TRACE_MSG(
			"Creating rotation '" << name << "' with values (" << values[0] << ", " << values[1] << ", " << values[2] << ").");

	matrix *= glm::yawPitchRoll(radians(values[1]), radians(values[0]), radians(values[2]));
	//matrix = glm::yawPitchRoll(radians(values[1]), radians(values[0]), radians(values[2])) * matrix;

	return true;
}

bool Transforms::parseTranslation(StringVectorIter& begin, StringVectorIter& end) {
	std::string& name = *begin++;

	float values[3] = { 0.0f, 0.0f, 0.0f };
	bool valid = true;
	if (name == "t") {
		valid = getNumbers(begin, end, 3, values);
	} else if (name == "tx") {
		valid = getNumbers(begin, end, 1, values);
	} else if (name == "ty") {
		valid = getNumbers(begin, end, 1, values + 1);
	} else if (name == "tz") {
		valid = getNumbers(begin, end, 1, values + 2);
	} else {
		REV_ERROR_MSG("Unknown command '" << name << "'!");
		return false;
	}

	if (!valid) {
		REV_ERROR_MSG("Command '" << name << "' is invalid!");
		return false;
	}

	REV_TRACE_MSG(
			"Creating translation '" << name << "' with values (" << values[0] << ", " << values[1] << ", " << values[2] << ").");

	//matrix = glm::translate(values[0], values[1], values[2]) * matrix;
	matrix *= glm::translate(values[0], values[1], values[2]);

	return true;
}

bool Transforms::parseScaling(StringVectorIter& begin, StringVectorIter& end) {
	std::string& name = *begin++;

	float values[3] = { 0.0f, 0.0f, 0.0f };
	bool valid = true;
	if (name == "s") {
		valid = getNumbers(begin, end, 3, values);
	} else if (name == "sa") {
		valid = getNumbers(begin, end, 1, values);
		values[1] = values[0];
		values[2] = values[0];
	} else if (name == "sx") {
		valid = getNumbers(begin, end, 1, values);
	} else if (name == "sy") {
		valid = getNumbers(begin, end, 1, values + 1);
	} else if (name == "sz") {
		valid = getNumbers(begin, end, 1, values + 2);
	} else {
		REV_ERROR_MSG("Unknown command '" << name << "'!");
		return false;
	}

	if (!valid) {
		REV_ERROR_MSG("Command '" << name << "' is invalid!");
		return false;
	}

	REV_TRACE_MSG(
			"Creating scaling '" << name << "' with values (" << values[0] << ", " << values[1] << ", " << values[2] << ").");

	//matrix = glm::scale(values[0], values[1], values[2]) * matrix;
	matrix *= glm::scale(values[0], values[1], values[2]);

	return true;
}

bool Transforms::toFloat(const std::string& str, float& result) {
	std::stringstream ss;
	ss << str;
	result = 0.0f;

	ss >> result;

	if (ss.good()) {
		REV_ERROR_MSG("ERROR: '" << str << "' is not a valid float number!");
		return false;
	} else if (result == 0.0f && str[0] != 0) {
		REV_ERROR_MSG("ERROR: '" << str << "' is not a valid float number!");
		return false;
	}

	return true;
}

} /* namespace rev */
