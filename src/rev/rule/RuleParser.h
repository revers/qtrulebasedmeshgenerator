/*
 * RuleParser.h
 *
 *  Created on: 14 lip 2013
 *      Author: Revers
 */

#ifndef RULEPARSER_H_
#define RULEPARSER_H_

#include "../xml/XMLRules.h"

namespace rev {

class RuleParser {
	RuleParser() = delete;
	~RuleParser() = delete;

public:
	static XMLRules* parse(const char* fileName);

};

} /* namespace rev */
#endif /* RULEPARSER_H_ */
