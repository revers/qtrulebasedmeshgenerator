/*
 * InstanceShape.h
 *
 *  Created on: 14 lip 2013
 *      Author: Revers
 */

#ifndef INSTANCESHAPE_H_
#define INSTANCESHAPE_H_

namespace rev {

enum class InstanceShape {
	NO_SHAPE, SPHERE, BOX, TUBE
};

} /* namespace rev */

#endif /* INSTANCESHAPE_H_ */
