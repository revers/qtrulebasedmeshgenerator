/*
 * XMLRules.h
 *
 *  Created on: 13 lip 2013
 *      Author: Revers
 */

#ifndef XMLRULES_H_
#define XMLRULES_H_

#include <rev/common/xml/RevXMLElement.h>
#include "XMLRule.h"

namespace rev {

class XMLRules: public XMLElement {
public:
	typedef std::list<XMLRule*> RuleList;

private:
	RuleList ruleList;
	int maxDepth = 0;

public:

	REV_DECLARE_XML_FACTORY(XMLRules, "rules");

	void mapXMLTagsToMembers() override {
		mapElement(&ruleList, XMLRule::getStaticElementName());
		mapAttribute(&maxDepth, "max_depth");
	}

	XMLRules() {
		initXMLElement();
	}

	~XMLRules() {
		for (auto aPtr : ruleList) {
			delete aPtr;
		}
	}

	int getMaxDepth() const {
		return maxDepth;
	}

	void setMaxDepth(int maxDepth) {
		this->maxDepth = maxDepth;
	}

	RuleList& getRuleList() {
		return ruleList;
	}

	void addRule(XMLRule* rule) {
		ruleList.push_back(rule);
	}

	friend std::ostream& operator<<(std::ostream &out, const XMLRules& xml) {
		out << "XMLRules[maxDepth=" << xml.maxDepth << ", ruleList=[";

		if (!xml.ruleList.empty()) {
			for (auto a : xml.ruleList) {
				out << *a << ", ";
			}
		}

		out << "] ]";
		return out;
	}

};

}
/* namespace rev */

#endif /* XMLRULES_H_ */
