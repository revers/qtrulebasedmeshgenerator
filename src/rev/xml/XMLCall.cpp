/*
 * XMLCall.cpp
 *
 *  Created on: 13 lip 2013
 *      Author: Revers
 */

#include "XMLCall.h"

namespace rev {

REV_IMPLEMENT_XML_FACTORY(XMLCall, "call");

} /* namespace rev */
