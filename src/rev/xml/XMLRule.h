/*
 * XMLRule.h
 *
 *  Created on: 13 lip 2013
 *      Author: Revers
 */

#ifndef XMLRULE_H_
#define XMLRULE_H_

#include <string>
#include <ostream>

#include <rev/common/xml/RevXMLElement.h>
#include "XMLInstance.h"
#include "XMLCall.h"

namespace rev {

class XMLRule: public XMLElement {
public:
	typedef std::list<XMLCall*> CallList;
	typedef std::list<XMLInstance*> InstanceList;

private:
	CallList callList;
	InstanceList instanceList;

	std::string name;
	std::string successor;
	int maxDepth = 0;
	int weight = 1;

public:

	REV_DECLARE_XML_FACTORY(XMLRule, "rule");

	void mapXMLTagsToMembers() override {
		mapElement(&callList, XMLCall::getStaticElementName());
		mapElement(&instanceList, XMLInstance::getStaticElementName());
		mapAttribute(&name, "name");
		mapAttribute(&successor, "successor");
		mapAttribute(&maxDepth, "max_depth");
		mapAttribute(&weight, "weight");
	}

	XMLRule() {
		initXMLElement();
	}

	~XMLRule() {
		for (auto aPtr : callList) {
			delete aPtr;
		}

		for (auto aPtr : instanceList) {
			delete aPtr;
		}
	}

	CallList& getCallList() {
		return callList;
	}

	void addCall(XMLCall* call) {
		callList.push_back(call);
	}

	InstanceList& getInstanceList() {
		return instanceList;
	}

	void addInstance(XMLInstance* instance) {
		instanceList.push_back(instance);
	}

	int getMaxDepth() const {
		return maxDepth;
	}

	void setMaxDepth(int maxDepth) {
		this->maxDepth = maxDepth;
	}

	const std::string& getName() const {
		return name;
	}

	void setName(const std::string& name) {
		this->name = name;
	}

	void setName(const char* name) {
		this->name = name;
	}

	const std::string& getSuccessor() const {
		return successor;
	}

	void setSuccessor(const std::string& successor) {
		this->successor = successor;
	}

	void setSuccessor(const char* successor) {
		this->successor = successor;
	}

	int getWeight() const {
		return weight;
	}

	void setWeight(int weight) {
		this->weight = weight;
	}

	friend std::ostream& operator<<(std::ostream &out, const XMLRule& xml) {
		out << "XMLRule[name=\"" << xml.name << "\", successor=\"" << xml.successor
		<< "\", maxDepth=" << xml.maxDepth << ", weight=" << xml.weight << ", callList=[";

		if (!xml.callList.empty()) {
			for (auto a : xml.callList) {
				out << *a << ", ";
			}
		}
		out << "], instanceList=[";

		if (!xml.instanceList.empty()) {
			for (auto a : xml.instanceList) {
				out << *a << ", ";
			}
		}
		out << "] ]";

		return out;
	}

};

}
/* namespace rev */

#endif /* XMLRULE_H_ */
