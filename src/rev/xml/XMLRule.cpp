/*
 * XMLRule.cpp
 *
 *  Created on: 13 lip 2013
 *      Author: Revers
 */

#include "XMLRule.h"

namespace rev {

REV_IMPLEMENT_XML_FACTORY(XMLRule, "rule");

} /* namespace rev */
