/*
 * XMLInstance.h
 *
 *  Created on: 13 lip 2013
 *      Author: Revers
 */

#ifndef XMLINSTANCE_H_
#define XMLINSTANCE_H_

#include <string>
#include <ostream>
#include <glm/glm.hpp>

#include <rev/common/xml/RevXMLElement.h>
#include "../rule/InstanceShape.h"

namespace rev {

class XMLInstance: public XMLElement {
	std::string shape;
	std::string transformsText;
	InstanceShape instanceShape = InstanceShape::NO_SHAPE;
	glm::mat4 matrix;

public:

	REV_DECLARE_XML_FACTORY(XMLInstance, "instance");

	XMLInstance() {
		initXMLElement();
	}

	~XMLInstance() {
	}

	void mapXMLTagsToMembers() override {
		mapAttribute(&shape, "shape");
		mapAttribute(&transformsText, "transforms");
	}

	const std::string& getShape() const {
		return shape;
	}

	void setShape(const char* shape) {
		this->shape = shape;
	}

	void setShape(const std::string& shape) {
		this->shape = shape;
	}

	const std::string& getTransformsText() const {
		return transformsText;
	}

	void setTransformsText(const char* transformsText) {
		this->transformsText = transformsText;
	}

	void setTransformsText(const std::string& transformsText) {
		this->transformsText = transformsText;
	}

	const glm::mat4& getMatrix() const {
		return matrix;
	}

	void setMatrix(const glm::mat4& matrix) {
		this->matrix = matrix;
	}

	InstanceShape getInstanceShape() const {
		return instanceShape;
	}

	void setInstanceShape(InstanceShape instanceShape) {
		this->instanceShape = instanceShape;
	}

	friend std::ostream& operator<<(std::ostream& out, const XMLInstance& xml) {
		out << "XMLInstance[shape=\"" << xml.shape << "\", transformsText=\""
		<< xml.transformsText << "\"]";

		return out;
	}

};

}
/* namespace rev */

#endif /* XMLINSTANCE_H_ */
