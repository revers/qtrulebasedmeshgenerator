/*
 * XMLInstance.cpp
 *
 *  Created on: 13 lip 2013
 *      Author: Revers
 */

#include "XMLInstance.h"

namespace rev {

REV_IMPLEMENT_XML_FACTORY(XMLInstance, "instance");

} /* namespace rev */
