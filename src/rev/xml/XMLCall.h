/*
 * XMLCall.h
 *
 *  Created on: 13 lip 2013
 *      Author: Revers
 */

#ifndef XMLCALL_H_
#define XMLCALL_H_

#include <string>
#include <ostream>
#include <glm/glm.hpp>

#include <rev/common/xml/RevXMLElement.h>

namespace rev {

class XMLCall: public XMLElement {

	std::string rule;
	std::string transformsText;
	int count = 1;
	glm::mat4 matrix;

public:

	REV_DECLARE_XML_FACTORY(XMLCall, "call");

	XMLCall() {
		initXMLElement();
	}

	~XMLCall() {
	}

	void mapXMLTagsToMembers() override {
		mapAttribute(&rule, "rule");
		mapAttribute(&transformsText, "transforms");
		mapAttribute(&count, "count");
	}

	const std::string& getRule() const {
		return rule;
	}

	void setRule(const char* rule) {
		this->rule = rule;
	}

	void setRule(const std::string& rule) {
		this->rule = rule;
	}

	const std::string& getTransformsText() const {
		return transformsText;
	}

	void setTransformsText(const char* transformsText) {
		this->transformsText = transformsText;
	}

	void setTransformsText(const std::string& transformsText) {
		this->transformsText = transformsText;
	}

	const glm::mat4& getMatrix() const {
		return matrix;
	}

	void setMatrix(const glm::mat4& matrix) {
		this->matrix = matrix;
	}

	int getCount() const {
		return count;
	}

	void setCount(int count) {
		this->count = count;
	}

	friend std::ostream& operator<<(std::ostream& out, const XMLCall& xml) {
		out << "XMLCall[count=" << xml.count << ", rule=\"" << xml.rule << "\", transformsText=\""
		<< xml.transformsText << "\"]";

		return out;
	}
};

}
/* namespace rev */

#endif /* XMLCALL_H_ */
